fenlibc - Fenix Standard C Library
==================================

fenlibc is a standard C library used by Fenix. This library follows the ISO C
and POSIX standards and is reasonably portable across UNIX and UNIX-like
systems, though it may not be suitable as a drop-in replacement for the C
library that exists on such a system.

Currently, the following headers are finished and fully compliant:
assert.h*
cpio.h
errno.h
float.h
iso646.h
libgen.h
limits.h??
stdbool.h
stddef.h
stdint.h
sys/un.h
tar.h

*relies on fprintf, which we don't have yet.

The following headers are POSIX-compliant, but not XSI-compliant:

The following headers are ISO-compliant, though not POSIX-compliant:

The following headers have working code, but are still in progress:
ctype.h (56%, missing locale-specific functions)
math.h (3%)
stdio.h (3% + most of printf and maybe perror?)
stdlib.h (13%)
string.h (26%)
time.h (4%)

The following headers exist and have all definitions, but no code:
aio.h
fmtmsg.h
fnmatch.h
grp.h
iconv.h
pwd.h
sys/utsname.h
syslog.h
ulimit.h

The following headers exist and are incomplete:
complex.h (<10%)
math.h (<33%)
netinit/in.h (37%)
stdio.h (<50%, ISO C functions are all there)
stdlib.h (<60%, ISO C functions are all there)
string.h (26%)
sys/io.h (??)
sys/socket.h (5%)
sys/types.h (<60%?)
sys/wait.h (63%)
time.h (14%)
unistd.h (<15%?)

Building
--------

Currently, fenlibc can be built as part of a standard Fenix ISO through its
makefile. To do so, simply run Fenix's build.sh script. It could use alterations
to allow its C library to be build to an existing system. If you'd like to
help out, that's a good place to start!

Portability
-----------

Currently, fenlibc has yet to be tested on any existing UNIX{,-like} systems.
If you have any luck getting it up and running on a UNIX{,-like} system,
let us know!

Copyright
---------

Licensed under the CWPL v5 with CC-BY 4.0 by Katlynn Richey.

fenlibc is free software. Feel free to modify and/or distribute it!
See the file COPYING for more details.