#include <time.h>
#include <kernel/cmos.h>

unsigned int get_days(unsigned int year, unsigned char month, unsigned char day) {
  unsigned int tot_days = day - 1;
  unsigned char month_days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  for(unsigned int i = 1970; i < year; i++) {
    if(i % 400 == 0 || (i % 100 != 0 && i % 4 == 0)) {
      tot_days += 366;
    }
    else {
      tot_days += 365;
    }
  }

  for(int i = 0; i < (month - 1); i++) {
    tot_days += month_days[i];
  }

  return tot_days;
}

time_t time(time_t * tloc) {
  time_t ret_val;
  
  ret_val = read_rtc();

  if(tloc != 0) {
    *tloc = ret_val;
  }
  return ret_val;
}
