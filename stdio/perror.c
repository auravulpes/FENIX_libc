#include <stdio.h>
#include <string.h>
#include <errno.h>

void perror(const char * str) {
  fprintf(stderr, "%s: %s\n", str, strerror(errno));
}
