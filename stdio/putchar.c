#include <stdio.h>

#ifdef __is_libk
#include <kernel/tty.h>
#endif

int putchar(int ic) {
#ifdef __is_libk
  char c = (char) ic;
  term_write(&c, sizeof(c));
#else
  /* TODO: Implement write system call to do this part */
#endif

  return ic;
}
