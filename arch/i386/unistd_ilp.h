#ifndef _ARCH_UNISTD_ILP_H
#define _ARCH_UNISTD_ILP_H

/*
  32-bit ISA, 32-bit int, long, pointer, and offset_t
*/
#define _POSIX_V6_ILP32_OFF32 1
#define _POSIX_V7_ILP32_OFF32 1

#endif