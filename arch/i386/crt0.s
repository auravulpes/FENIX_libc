	.section .text
	.global _start
_start:
	# Set up end of stack frame linked list
	movl $0, %ebp
	pushl %ebp # rip = 0
	pushl %ebp # ebp = 0
	movl %esp, %ebp

	pushl %esi
	pushl %edi

	# Prepare signals, memory alloc, stdio, etc.
	call initialize_standard_library

	# Run global constructors
	call _init

	# restore argc/argv
	popl %edi
	popl %esi

	# Run main
	call main

	# Terminate with exit code
	movl %eax, %edi
	call exit
	.size _start, . - _start
