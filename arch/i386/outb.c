#include <sys/io.h>

/*
  This was also practically lifted
  from musl.
  -Kat
 */
void outb(unsigned char val, unsigned short port) {
  __asm__ volatile("outb %0,%1" : : "a" (val), "dN" (port));
}