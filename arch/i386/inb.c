#include <sys/io.h>

/*
  This was practically copied from musl, but it's 
  not like there's much there to copy anyways.
  -Kat
 */
unsigned char inb(unsigned short int port) {
  unsigned short val;
  __asm__ volatile("inb %1,%0" : "=a" (val) : "dN" (port));
  return val;
}