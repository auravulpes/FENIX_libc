/*
 * <ctype.h> - character types
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _CTYPE_H
#define _CTYPE_H

#include <types/locale_t.h>

/* C standard functions */
int isalnum(int);
int isalpha(int);
int isblank(int);
int iscntrl(int);
int isdigit(int);
int isgraph(int);
int islower(int);
int isprint(int);
int ispunct(int);
int isspace(int);
int isupper(int);
int isxdigit(int);

/* POSIX extensions */
#ifdef _POSIX_C_SOURCE
int isalnum_l(int, locale_t);
int isalpha_l(int, locale_t);
int isblank_l(int, locale_t);
int iscntrl_l(int, locale_t);
int isdigit_l(int, locale_t);
int isgraph_l(int, locale_t);
int islower_l(int, locale_t);
int isprint_l(int, locale_t);
int ispunct_l(int, locale_t);
int isspace_l(int, locale_t);
int isupper_l(int, locale_t);
int isxdigit_l(int, locale_t);
#endif

/* C standard stuff */
int tolower(int);
int toupper(int);

/* POSIX extensions */
#ifdef _POSIX_C_SOURCE
int tolower_l(int, locale_t);
int toupper_l(int, locale_t);
#endif

#ifdef _XOPEN_SOURCE
/* Obsolete XSI stuff */
int isascii(int);
int toascii(int);
/* Obsolete XSI macros */
#define _toupper(x) toupper(x)
#define _tolower(x) tolower(x)
#endif

#endif
