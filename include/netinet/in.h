/*
 * <netinet/in.h> - internet address stuff
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _NETINET_IN_H
#define _NETINET_IN_H

#include <inttypes.h>
#include <sys/socket.h>

#ifndef __have_in_port_t
#define __have_in_port_t
typedef uint16_t in_port_t;
#endif

#ifndef __have_in_addr_t
#define __have_in_addr_t
typedef uint32_t in_addr_t;
#endif

struct in_addr {
  in_addr_t s_addr;
};

struct sockaddr_in{
  sa_family_t sin_family;
  in_port_t sin_port;
  struct in_addr sin_addr;
};

struct in6_addr {
  uint8_t s6_addr[16];
};

typedef struct _ipv6_sockaddr_in {
  sa_family_t sin6_family;
  in_port_t sin6_port;
  uint32_t sin6_flowinfo;
  struct in6_addr sin6_addr;
  uint32_t sin6_scope_id;
} sockaddr_in6;

extern const struct in6_addr in6addr_any;
extern const struct in6_addr in6addr_loopback;
/* Need INIT macros for the above */

struct ipv6_mreq {
  struct int6_addr ipv6mr_multiaddr;
  unsigned ipv6mr_interface;
};

#define IPPROTO_IP 4
#define IPPROTO_IPV6 6
#define IPPROTO_ICMP 1
#define IPPROTO_RAW 2
#define IPPROTO_TCP 5
#define IPPROTO_UDP 7

#endif
