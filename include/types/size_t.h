/*
 * <types/size_t.h> - size of objects
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _TYPES_SIZE_T_H
#define _TYPES_SIZE_T_H

typedef unsigned int size_t;

#endif
