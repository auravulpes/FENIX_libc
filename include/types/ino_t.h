/*
 * <types/ino_t.h> - file serial number type
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _TYPES_INO_T_H
#define _TYPES_INO_T_H

typedef unsigned long long int ino_t;

#endif