/*
 * <types/gid_t.h> - group ID type
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _TYPES_GID_T_H
#define _TYPES_GID_T_H

#include <types/id_t.h>

typedef unsigned id_t gid_t;

#endif
