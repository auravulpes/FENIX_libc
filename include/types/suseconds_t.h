/*
 * <types/suseconds_t.h> - time in microseconds
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _TYPES_SUSECONDS_T_H
#define _TYPES_SUSECONDS_T_H

typedef long long int suseconds_t;

#endif /* not _HEADER */
