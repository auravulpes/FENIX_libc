/*
 * <tar.h> - tar definitions
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _TAR_H
#define _TAR_H

#define TMAGIC "ustar"
#define TMAGLEN 6
#define TVERSION "00"
#define TVERSLEN 2

#define REGTYPE '0' /* Regular file */
#define AREGTYPE '\0' /* Regular file */
#define LNKTYPE '1' /* Link */
#define SYMTYPE '2' /* Symbolic link */
#define CHRTYPE '3' /* Character special */
#define BLKTYPE '4' /* Block special */
#define DIRTYPE '5' /* Directory */
#define FIFOTYPE '6' /* FIFO */
#define CONTTYPE '7' /* Reserved */

#define TSUID   04000 /* Set UID on exec. */
#define TSGID   02000 /* Set GID on exec. */
#define TSVTX   01000 /* On dirs, restricted deletion flag */
#define TUREAD  00400 /* Read by owner */
#define TUWRITE 00200 /* Write by owner */
#define TUEXEC  00100 /* Exec./search by owner */
#define TGREAD  00040 /* Read by group */
#define TGWRITE 00020 /* Write by group */
#define TGEXEC  00010 /* Exec./search by group */
#define TOREAD  00004 /* Read by others */
#define TOWRITE 00002 /* Write by others */
#define TOEXEC  00001 /* Exec./search by others */

#endif
