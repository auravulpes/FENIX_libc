/*
 * <float.h> - floating point stuffs
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _FLOAT_H
#define _FLOAT_H

#if __STDC_HOSTED__
#define FLT_ROUNDS -1 /* This needs to reflect fesetround() in <fenv.h> */
#else
#define FLT_ROUNDS 1 /* For freestanding, just round to nearest */
#endif

#define FLT_EVAL_METHOD 0

#define FLT_RADIX 2

/*
  Okay, hold up. What is going on with LDBL? That...shouldn't be right. Ugh.
  -Kat
*/

#define FLT_MANT_DIG 24
#define DBL_MANT_DIG 53
#define LDBL_MANT_DIG 113

#define DECIMAL_DIG 17

#define FLT_DIG 6
#define DBL_DIG 15
#define LDBL_DIG 10

#define FLT_MIN_EXP -125
#define DBL_MIN_EXP -1021
#define LDBL_MIN_EXP -120

#define FLT_MAX_EXP +128
#define DBL_MAX_EXP +1024
#define LDBL_MAX_EXP +122

#define FLT_MIN_10_EXP -37
#define DBL_MIN_10_EXP -307
#define LDBL_MIN_10_EXP -37

#define FLT_MAX_10_EXP +38
#define DBL_MAX_10_EXP +308
#define LDBL_MAX_10_EXP +37

#define FLT_MAX 3.40282347E+38F
#define DBL_MAX 1.7976931348623157E+308
#define LDBL_MAX 1E+37

#define FLT_EPSILON 1.19209290E-07F
#define DBL_EPSILON 2.2204460492503131E-16
#define LDBL_EPSILON 1E-9

#define FLT_MIN 1.17549435E-38F
#define DBL_MIN 2.2250738585072014E-308
#define LDBL_MIN 1E-37

#endif
