/*
 * <limits.h> - implementation-defined constants
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _FLC_LIMITS
#define _FLC_LIMITS


#if defined(_POSIX_C_SOURCE) || defined(_XOPEN_SOURCE)
/* 
  Minimum values (POSIX)
  Included in case anyone wants/needs 'em.
*/
#define _POSIX_AIO_LISTIO_MAX 2
#define _POSIX_AIO_MAX 1
#define _POSIX_ARG_MAX 4096
#define _POSIX_CHILD_MAX 25
#define _POSIX_DELAYTIMER_MAX 32
#define _POSIX_HOST_NAME_MAX 255
#define _POSIX_LINK_MAX 8
#define _POSIX_LOGIN_NAME_MAX 9
#define _POSIX_MAX_CANON 255
#define _POSIX_MAX_INPUT 255
#define _POSIX_MQ_OPEN_MAX 8
#define _POSIX_MQ_PRIO_MAX 32
#define _POSIX_NAME_MAX 14
#define _POSIX_NGROUPS_MAX 8
#define _POSIX_OPEN_MAX 20
#define _POSIX_PATH_MAX 256
#define _POSIX_PIPE_BUF 512
#define _POSIX_RE_DUP_MAX 255
#define _POSIX_RTSIG_MAX 8
#define _POSIX_SEM_NSEMS_MAX 256
#define _POSIX_SEM_VALUE_MAX 32767
#define _POSIX_SIGQUEUE_MAX 32
#define _POSIX_SSIZE_MAX 32767
#define _POSIX_SS_REPL_MAX 4
#define _POSIX_STREAM_MAX 8
#define _POSIX_SYMLINK_MAX 255
#define _POSIX_SYMLOOP_MAX 8
#define _POSIX_THREAD_DESTRUCTOR_ITERATIONS 4
#define _POSIX_THREAD_KEYS_MAX 128
#define _POSIX_THREAD_THREADS_MAX 64
#define _POSIX_TIMER_MAX 32
#define _POSIX_TRACE_EVENT_NAME_MAX 30
#define _POSIX_TRACE_NAME_MAX 8
#define _POSIX_TRACE_SYS_MAX 8
#define _POSIX_TRACE_USER_EVENT_MAX 32
#define _POSIX_TTY_NAME_MAX 9
#define _POSIX_TZNAME_MAX 6
#define _POSIX2_BC_BASE_MAX 99
#define _POSIX2_BC_DIM_MAX 2048
#define _POSIX2_BC_SCALE_MAX 99
#define _POSIX2_BC_STRING_MAX 1000
#define _POSIX2_CHARCLASS_NAME_MAX 14
#define _POSIX2_COLL_WEIGHTS_MAX 2
#define _POSIX2_EXPR_NEST_MAX 32
#define _POSIX2_LINE_MAX 2048
#define _POSIX2_RE_DUP_MAX 255
#define _XOPEN_IOV_MAX 16
#define _XOPEN_NAME_MAX 255
#define _XOPEN_PATH_MAX 1024

/* Pathname Variable Values */
#define FILESIZEBITS 64
#define LINK_MAX 16
/*
  Remember, you only get 1024 canons.
  After that, new canons have to start
  merging in with old ones. Hence,
  Smash Ultimate.

  -Kat
*/
#define MAX_CANON 1024
#define MAX_INPUT 256
#define NAME_MAX 256
#define PATH_MAX 4096
#define PIPE_BUF 1024
#define POSIX_ALLOC_SIZE_MIN 1024
#define POSIX_REC_INCR_XFER_SIZE 512
#define POSIX_REC_MAX_XFER_SIZE 1048576
#define POSIX_REC_MIN_XFER_SIZE 512
#define POSIX_REC_XFER_ALIGN 512
#define SYMLINK_MAX 2048

/* Runtime Increasable Values */

/*
  So, when we do <unistd.h>, that makes these weird.
  Fun. Fun, fun.
  -Kat
*/

#define BC_BASE_MAX 256
#define BC_DIM_MAX 8192
#define BC_SCALE_MAX 99
#define BC_STRING_MAX 1024
#define CHARCLASS_NAME_MAX 32
#define COLL_WEIGHTS_MAX 4
#define EXPR_NEST_MAX 128
#define LINE_MAX 8192
#define RE_DUP_MAX 512

/* Maximum values */
#define _POSIX_CLOCKRES_MIN 20000000

/* ssize_t size limit*/
#define SSIZE_MAX 32767

/* Miscellaneous invariants */
#define NL_ARGMAX 9
#define NL_LANGMAX 14
#define NL_MSGMAX 65535
#define NL_SETMAX 255
#define NL_TEXTMAX 8192
#define NZERO 20

#endif

/* Numerical limits */
/* Shorts (16-bit in both ISO and POSIX) */
#define SHRT_MAX 32767
#define SHRT_MIN -32767
#define USHRT_MAX 65535
/* Integers */
#ifdef _POSIX_C_SOURCE
/* If we're POSIX, int is 32-bit */
#define WORD_BIT 32
#define INT_MAX 2147483647
#define INT_MIN -2147483648
#define UINT_MAX 4294967295U
#else
/* If we're not POSIX, int is only 16-bit, for some reason. */
#define INT_MAX 32767
#define INT_MIN -32767
#define UINT_MAX 65535U
#endif
/* Long ints */
#ifdef _POSIX_C_SOURCE
/* If we're POSIX, long is 64-bit */
#define LONG_BIT 64
#define LONG_MAX 9223372036854775807L
#define LONG_MIN -9223372036854775808L
#define ULONG_MAX 18446744073709551615UL
#else
/* If we're not POSIX, long is only 32-bit */
#define LONG_MAX 2147483647L
#define LONG_MIN -2147483647L
#define ULONG_MAX 4294967295UL
#endif
/* Long long ints (64-bit in both ISO and POSIX) */
#define LLONG_MAX 9223372036854775807LL
#define LLONG_MIN -9223372036854775808LL
#define ULLONG_MAX 18446744073709551615ULL
/* Characters (Defined as 8 bit) */
#define CHAR_BIT 8
#if '\xff' > 0
#define CHAR_MAX 255
#define CHAR_MIN 0
#else
#define CHAR_MAX 127
#define CHAR_MIN -128
#endif
#define SCHAR_MAX 127
#define SCHAR_MIN -128
#define UCHAR_MAX 255
#define MB_LEN_MAX 1

#endif /* ndef _FLC_LIMITS */
