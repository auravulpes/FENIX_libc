/*
 * <fmtmsg.h> - message display structures
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _FMTMSG_H
#define _FMTMSG_H

#define MM_HARD 01 /* Source is hardware */
#define MM_SOFT 02 /* Source is software */
#define MM_FIRM 04 /* Source is firmware */

#define MM_APPL  010 /* Detected by application */
#define MM_UTIL  020 /* Detected by utility */
#define MM_OPSYS 040 /* Detected by operating system */

#define MM_RECOVER 0100 /* Recoverable */
#define MM_NRECOV  0200 /* Non-recoverable */

#define MM_HALT    01000 /* Caused application to halt */
#define MM_ERROR   02000 /* A non-fatal fault */
#define MM_WARNING 04000 /* An unusual non-error condition */
#define MM_INFO    010000 /* Informative message */
#define MM_NOSEV   020000 /* No severity provided */

#define MM_PRINT   0100000 /* Display on stderr */
#define MM_CONSOLE 0200000 /* Display on system console */

#define MM_NULLLBL (char *) 0 /* Null label */
#define MM_NULLSEV (int) 0 /* Null severity */
#define MM_NULLMC  (long) 0L /* Null class */
#define MM_NULLTXT (char *) 0 /* Null message text */
#define MM_NULLACT (char *) 0 /* Null action */
#define MM_NULLTAG (char *) 0 /* Null tag */

#define MM_OK    0 /* Function succeeded */
#define MM_NOTOK 1 /* Function failed completely */
#define MM_NOMSG 2 /* Could not generate message on stderr */
#define MM_NOCON 3 /* Could not generate message on console */

int fmtmsg(long, const char *, int, const char *, const char *, const char *);

#endif
