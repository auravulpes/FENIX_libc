/*
 * <stdio.h> - input/output
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _STDIO_H
#define _STDIO_H

#ifdef _XOPEN_SOURCE
#ifdef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif
#endif

#include <sys/cdefs.h>

#ifndef NULL
#define NULL ((void *) 0)
#endif

#include <types/size_t.h>
#include <types/ssize_t.h>
#include <types/off_t.h>

#ifdef _POSIX_C_SOURCE
#include <types/ssize_t.h>
#endif

#include <stdarg.h>

typedef unsigned long int fpos_t;

typedef struct _FILE_STRUCT {
  fpos_t position; /* Current position in file */
  int fildes; /* The file's file descriptor */
  int rw; /* Is it read-only or read-write? */
  int errind; /* Have we got an error? */
  int eofind; /* Have we reached EOF? */
  char * buffer; /* The file's buffer */
  int buf_size; /* The size of the file's buffer */
  int buf_mode; /* How is the file buffered? (_IOFBF, etc.) */
} FILE;

#define EOF (-1)

#define _IOFBF 01 /* IO fully buffered */
#define _IOLBF 02 /* IO line buffered */
#define _IONBF 04 /* IO not buffered */

#define SEEK_CUR 01 /* Seek relative to current position */
#define SEEK_END 02 /* Seek relative to end of file */
#define SEEK_SET 04 /* Seek relative to start of file */

#define BUFSIZ 1024
#define FILENAME_MAX 4096
#define FOPEN_MAX 1024
#define L_tmpnam 512
#define TMP_MAX 65536
#ifdef _POSIX_C_SOURCE
#define L_ctermid 1024
#endif

extern const FILE * stdin;
extern const FILE * stdout;
extern const FILE * stderr;

#define stdin (stdin)
#define stdout (stdout)
#define stderr (stderr)

/* Operations on files (ISO C Std. 7.19.4) */
int remove(const char *);
int rename(const char *, const char *);

FILE * tmpfile(void);
char * tmpnam(char *);

/* File access functions (ISO C Std. 7.19.5) */
int fclose(FILE *);
int fflush(FILE *);
FILE * fopen(const char * restrict, const char * restrict);
FILE * freopen(const char * restrict, const char * restrict, FILE * restrict);

void setbuf(FILE * restrict, char * restrict);
int setvbuf(FILE * restrict, char * restrict, int, size_t);

/* Formatted IO functions (ISO C Std. 7.19.6) */
int fprintf(FILE * restrict, const char * restrict, ...);
int printf(const char * restrict, ...);
int sprintf(char * restrict, const char * restrict, ...);
int snprintf(char * restrict, size_t n, const char * restrict, ...);
int vfprintf(FILE * restrict, const char * restrict, va_list);
int vprintf(const char * restrict, va_list);
int vsnprintf(char * restrict, size_t n, const char * restrict, va_list);
int vsprintf(char * restrict, const char * restrict, va_list);

int fscanf(FILE * restrict, const char * restrict, ...);
int scanf(const char * restrict, ...);
int sscanf(const char * restrict, const char * restrict, ...);
int vfscanf(FILE * restrict, const char * restrict, va_list);
int vscanf(const char * restrict, va_list);
int vsscanf(const char * restrict, const char * restrict, va_list);

/* Character IO functions (ISO C Std. 7.19.7) */
int fgetc(FILE *);
int fgets(char * restrict, int, FILE * restrict);
int fputc(int, FILE *);
int fputs(const char * restrict, FILE * restrict);

#define getc(stream) fgetc(stream)
int getchar(void);
char * gets(char *);
int putc(int, FILE *);
int putchar(int);
int puts(const char *);

int ungetc(int, FILE *);

/* Direct IO functions (ISO C Std. 7.19.8) */
size_t fread(void * restrict, size_t, size_t, FILE * restrict);
size_t fwrite(const void * restrict, size_t, size_t, FILE * restrict);

/* File positioning functions (ISO C Std. 7.19.9) */
int fgetpos(FILE * restrict, fpos_t * restrict);
int fseek(FILE *, long int, int);
int fsetpos(FILE *, const fpos_t *);
long int ftell(FILE *);
void rewind(FILE *);

/* Error-handling functions (ISO C Std. 7.19.10) */
void clearerr(FILE *);
int feof(FILE *);
int ferror(FILE *);

void perror(const char *);

#endif
