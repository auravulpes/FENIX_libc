/*
 * <libgen.h> - string parsing
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _LIBGEN_H
#define _LIBGEN_H

#include <stdlib.h>
#include <string.h>
#include <errno.h>

char * basename(char *);
char * dirname(char *);

#endif
