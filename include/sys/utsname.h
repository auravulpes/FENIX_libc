/*
 * <sys/utsname.h> - system information
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _SYS_UTSNAME_H
#define _SYS_UTSNAME_H

struct utsname {
  char sysname[70];
  char nodename[70];
  char release[70];
  char version[70];
  char machine[70];
};

int uname(struct utsname *);

#endif
