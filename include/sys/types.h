/*
 * <sys/types.h> - data types
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _SYS_TYPES_H
#define _SYS_TYPES_H

#include <types/size_t.h>
#include <types/ssize_t.h>

#include <types/wchar_t.h>

#include <types/off_t.h>
#include <types/mode_t.h>
#include <types/nlink_t.h>

#include <types/blkcnt_t.h>
#include <types/blksize_t.h>

#include <types/fsblkcnt_t.h>
#include <types/fsfilcnt_t.h>
#include <types/ino_t.h>

#include <types/id_t.h>
#include <types/uid_t.h>
#include <types/gid_t.h>
#include <types/pid_t.h>

#include <types/susecond_t.h>
#include <types/time_t.h>
#include <types/clock_t.h>

#include <types/clockid_t.h>
#include <types/timer_t.h>

#endif /* not _HEADER */
