/*
 * <sys/wait.h> - declarations for waiting
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _SYS_WAIT_H
#define _SYS_WAIT_H

/* Constants for waitpid() */
#define WCONTINUED 1
#define WNOHANG    2
#define WUNTRACED  3

/* Options for waitid() */
#define WEXITED  1
#define WNOWAIT  2
#define WSTOPPED 3

enum idtype_t {P_ALL, P_PGID, P_PID};

#include <types/pid_t.h>
#include <signal.h>

pid_t wait(int *);
int waitid(idtype_t, id_t, siginfo_t *, int);
pid_t waitpid(pid_t, int *, int);

#endif
