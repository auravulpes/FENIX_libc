/*
 * <sys/socket.h> - main sockets header
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _SYS_SOCKET_H
#define _SYS_SOCKET_H

typedef long int socklen_t;
#include <types/sa_family_t.h>

struct sockaddr {
  sa_family_t sa_family;
  char        sa_data[];
};


#endif
