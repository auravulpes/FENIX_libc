/*
 * <sys/un.h> - definitions for UNIX domain sockets
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _SYS_UN_H
#define _SYS_UN_H

#include <types/sa_family_t.h>

struct sockaddr_un {
  sa_family_t sun_family
  char        sun_path[108];
};

#endif
