/*
 * <iconv.h> - codeset conversion
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _ICONV_H
#define _ICONV_H

typedef void * iconv_t;
#include <types/size_t.h>

size_t iconv(iconv_t, char ** restrict, size_t * restrict, 
             char ** restrict, size_t * restrict);
int iconv_close(iconv_t);
iconv_t iconv_open(const char *, const char *);

#endif