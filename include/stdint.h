#ifndef _STDINT_H
#define _STDINT_H

typedef signed char int8_t;
typedef signed short int int16_t;
typedef signed int int32_t;
typedef signed long int int64_t;

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long int uint64_t;

typedef signed short int int_least8_t;
typedef signed int int_least16_t;
typedef signed long int int_least32_t;
typedef signed long long int int_least64_t;

typedef unsigned short int uint_least8_t;
typedef unsigned int uint_least16_t;
typedef unsigned long int uint_least32_t;
typedef unsigned long long int uint_least64_t;

typedef int8_t int_fast8_t;
typedef int16_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef int64_t int_fast64_t;

typedef uint8_t uint_fast8_t;
typedef uint16_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
typedef uint64_t uint_fast64_t;

typedef signed int intptr_t;
typedef unsigned int uintptr_t;

typedef long long int intmax_t;
typedef unsigned long long int uintmax_t;

#define INT8_MIN -0x80
#define INT8_MAX 0x7F
#define UINT8_MAX 0xFF
#define INT16_MIN -0x8000
#define INT16_MAX 0x7FFF
#define UINT16_MAX 0xFFFF
#define INT32_MIN -0x80000000
#define INT32_MAX 0x7FFFFFFF
#define UINT32_MAX 0xFFFFFFFF
#define INT64_MIN -0x8000000000000000
#define INT64_MAX 0x7FFFFFFFFFFFFFFF
#define UINT64_MAX 0xFFFFFFFFFFFFFFFF

#define INT_LEAST8_MIN -0x80
#define INT_LEAST8_MAX 0x7F
#define UINT_LEAST8_MAX 0xFF
#define INT_LEAST16_MIN -0x8000
#define INT_LEAST16_MAX 0x7FFF
#define UINT_LEAST16_MAX 0xFFFF
#define INT_LEAST32_MIN -0x80000000
#define INT_LEAST32_MAX 0x7FFFFFFF
#define UINT_LEAST32_MAX 0xFFFFFFFF
#define INT_LEAST64_MIN -0x8000000000000000
#define INT_LEAST64_MAX 0x7FFFFFFFFFFFFFFF
#define UINT_LEAST64_MAX 0xFFFFFFFFFFFFFFFF

#define INT_FAST8_MIN -0x80
#define INT_FAST8_MAX 0x7F
#define UINT_FAST8_MAX 0xFF
#define INT_FAST16_MIN -0x8000
#define INT_FAST16_MAX 0x7FFF
#define UINT_FAST16_MAX 0xFFFF
#define INT_FAST32_MIN -0x80000000
#define INT_FAST32_MAX 0x7FFFFFFF
#define UINT_FAST32_MAX 0xFFFFFFFF
#define INT_FAST64_MIN -0x8000000000000000
#define INT_FAST64_MAX 0x7FFFFFFFFFFFFFFF
#define UINT_FAST64_MAX 0xFFFFFFFFFFFFFFFF

#define INTPTR_MIN -0x8000
#define INTPTR_MAX 0x7FFF
#define UINTPTR_MAX 0xFFFF

#define PTRDIFF_MIN -65535
#define PTRDIFF_MAX 65535

#define SIZE_MAX 65535

#define WCHAR_MIN 0
#if defined(_POSIX_C_SOURCE) || defined(_XOPEN_SOURCE)
#define WCHAR_MAX 0xFFFFFFFF
#else
#define WCHAR_MAX 0xFF
#endif

#define INT8_C(x) ((int_least8_t) x)
#define INT16_C(x) ((int_least16_t) x)
#define INT32_C(x) ((int_least32_t) x)
#define INT64_C(x) ((int_least64_t) x)
#define UINT8_C(x) ((uint_least8_t) x)
#define UINT16_C(x) ((uint_least16_t) x)
#define UINT32_C(x) ((uint_least32_t) x)
#define UINT64_C(x) ((uint_least64_t) x)

#define INTMAX_C(x) ((intmax_t) x)
#define UINTMAX_C(x) ((uintmax_t) x)

#endif