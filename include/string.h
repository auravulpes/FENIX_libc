/*
 * <string.h> - string handling
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _STRING_H
#define _STRING_H

#include <sys/cdefs.h>

#include <types/size_t.h>
#ifdef _POSIX_C_SOURCE
#include <types/locale_t.h>
#endif

#ifndef NULL
#define NULL (void *) 0
#endif

/* Copying Functions (ISO C Std. 7.24.2) */
void * memcpy(void * __restrict, const void * __restrict, size_t);
void * memmove(void *, const void *, size_t);

char * strcpy(char * restrict, const char * restrict);
char * strncpy(char * restrict, const char * restrict, size_t);

/* Concatenation Functions (ISO C Std. 7.24.3) */
char * strcat(char * restrict, const char * restrict);
char * strncat(char * restrict, const char * restrict, size_t);

/* Comparison Functions (ISO C Std. 7.24.4) */
int memcmp(const void *, const void *, size_t);
int strcmp(const char *, const char *);
int strncmp(const char *, const char *, size_t);

int strcoll(const char *, const char *);
size_t strxfrm(char * restrict, const char * restrict, size_t);

/* Search Functions (ISO C Std. 7.24.5) */
void * memchr(const void *, int, size_t);
char * strchr(const char *, int);

size_t strcspn(const char *, const char *);
char * strpbrk(const char *, const char *);
char * strrchr(const char *, int);
size_t strspn(const char *, const char *);
char * strstr(const char *, const char *);

char * strtok(char * restrict, char * restrict);

/* Miscellaneous Functions (ISO C Std. 7.24.6) */
void * memset(void *, int, size_t);
char * strerror(int);
size_t strlen(const char *);

#endif
