/*
 * <ulimit.h> - ulimit
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _ULIMIT_H
#define _ULIMIT_H

#define UL_GETFSIZE 1
#define UL_SETFSIZE 2

long int ulimit(int, ...);

#endif
