/*
 * <math.h> - mathematics
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v5. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _MATH_H
#define _MATH_H

#include <limits.h>

#if FLT_EVAL_METHOD == 1
typedef double float_t;
typedef double double_t;
#elseif FLT_EVAL_METHOD == 2
typedef long double float_t;
typedef long double double_t;
#else
typedef float float_t;
typedef double double_t;
#endif

/* Both of these are basically lifted from musl. */
#define INFINITY (1e5000)
#define NAN (0.0f / 0.0f)

/* iirc, this is how musl does it. */
#define HUGE_VAL INFINITY
#define HUGE_VALF ((float) INFINITY)
#define HUGE_VALL ((long double) INFINITY)

#define M_E 2.718281828
#define M_PI 3.14159265
#define PI 3.14159265
#define M_PI_2 1.5707963268
#define M_PI_4 0.7853981634
#define M_1_PI 0.318309886184
#define M_2_PI 0.636619772368
#define M_2_SQRTPI 1.1283791671
#define M_SQRT2 1.41421856237
#define M_SQRT1_2 0.707106781187
#define M_LOG2E 1.44269504089
#define M_LOG10E 0.4342944819
#define M_LN2 0.69314718056
#define M_LN10 2.302585093

#define MATH_ERRNO 1
#define MATH_ERREXCEPT 2
#define math_errhandling MATH_ERRNO

#define FP_ILOGB0 INT_MIN
#define FP_ILOGBNAN INT_MAX

#define FP_INFINITE 1
#define FP_NAN 2
#define FP_NORMAL 3
#define FP_SUBNORMAL 4
#define FP_ZERO 0

/* Classification Macros 7.12.3 */

int __fpclassifyd(double x);
int __fpclassifyf(float x);
int __fpclassifyl(long double x);

/* We're just gonna use the approach in the C standard here */
#define fpclassify(x) ((sizeof(x) == sizeof(float)) ? __fpclassifyf(x) : \
                      (sizeof(x) == sizeof(double)) ? __fpclassifyd(x) : \
                      __fpclassifyl(x))

#define isfinite(x) ((fpclassify(x) != FP_INFINITE) && (fpclassify(x) != FP_NAN))
#define isinf(x) (fpclassify(x) == FP_INFINITE)
#define isnan(x) (fpclassify(x) == FP_NAN)
#define isnormal(x) (fpclassify(x) == FP_NORMAL)

int __signbitd(double x);
int __signbitf(float x);
int __signbitl(long double x);

/*
  In theory, I could just use (x < 0 || x == -0.0f), but would that work for
  infinity and NaN? I feel like no. So fuck it. We'll just write some small
  functions to pull the sign bit straight from the number.
  -Kat
*/
#define signbit(x) ((sizeof(x) == sizeof(float)) ? __signbitf(x) : \
                    (sizeof(x) == sizeof(double)) ? __signbitd(x) : \
                    __signbitl(x))

/* Trigonometric Functions (ISO C Std. 7.12.4) */

double acos(double);
float acosf(float);
long double acosl(long double);

double asin(double);
float asinf(float);
long double asinl(long double);

double atan(double);
float atanf(float);
long double atanl(long double);

double atan2(double);
float atan2f(float);
long double atan2l(long double);

double cos(double);
float cosf(float);
long double cosl(long double);

double sin(double);
float sinf(float);
long double sinl(long double);

double tan(double);
float tanf(float);
long double tanl(long double);

/* Hyperbolic Functions (ISO C Std. 7.12.5) */

double acosh(double);
float acoshf(float);
long double acoshl(long double);

double asinh(double);
float asinhf(float);
long double asinhl(long double);

double atanh(double);
float atanhf(float);
long double atanhl(long double);

double cosh(double);
float coshf(float);
long double coshl(long double);

double sinh(double);
float sinhf(float);
long double sinhl(long double);

double tanh(double);
float tanhf(float);
long double tanhl(long double);

/* Exponential and Logarithmic Functions (ISO C Std. 7.12.6) */

double exp(double);
float expf(float);
long double expl(long double);

double exp2(double);
float exp2f(float);
long double exp2l(long double);

double expm1(double);
float expm1f(float);
long double expm1l(long double);

double frexp(double, int *);
float frexpf(float, int *);
long double frexpl(long double, int *);

int ilogb(double);
int ilogbf(float);
int ilogbl(long double);

double ldexp(double, int);
float ldexpf(float, int);
long double ldexpl(long double, int);

double log(double);
float logf(float);
long double logl(long double);

double log10(double);
float log10f(float);
long double log10l(long double);

double log1p(double);
float log1pf(float);
long double log1pl(long double);

double log2(double);
float log2f(float);
long double log2l(long double);

double logb(double);
float logbf(float);
long double logbl(long double);

double modf(double, double *);
float modff(float, float *);
long double modfl(long double, long double *);

double scalbn(double, int);
float scalbnf(float, int);
long double scalbnl(long double, int);

double scalbln(double, long);
float scalblnf(float, long);
long double scalblnl(long double, long);

/* Power and Absolute Value Functions (ISO C Std. 7.12.7) */

double fabs(double);
float fabsf(float);
long double fabsl(long double);



#endif
