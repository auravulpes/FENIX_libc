/*
 * <syslog.h> - system error logging
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _SYSLOG_H
#define _SYSLOG_H

/* logopt option for openlog() */
#define LOG_PID    001
#define LOG_CONS   002
#define LOG_NDELAY 004
#define LOG_ODELAY 010
#define LOG_NOWAIT 020

/* facility argument for openlog() */
#define LOG_KERN   1
#define LOG_USER   2
#define LOG_MAIL   3
#define LOG_NEWS   4
#define LOG_UUCP   5
#define LOG_DAEMON 6
#define LOG_AUTH   7
#define LOG_CRON   8
#define LOG_LPR    9
#define LOG_LOCAL0 10
#define LOG_LOCAL1 11
#define LOG_LOCAL2 12
#define LOG_LOCAL3 13
#define LOG_LOCAL4 14
#define LOG_LOCAL5 15
#define LOG_LOCAL6 16
#define LOG_LOCAL7 17

/* priority argument for syslog() */
#define LOG_EMERG   1
#define LOG_CRIT    2
#define LOG_ALERT   3
#define LOG_ERROR   4
#define LOG_WARNING 5
#define LOG_NOTICE  6
#define LOG_INFO    7
#define LOG_DEBUG   8

/* I'm not 100% sure how this is supposed to work -Kat */
#define LOG_MASK(pri) (pri)

void closelog(void);
void openlog(const char *, int, int);
int setlogmask(int);
void syslog(int, const char *, ...);

#endif 
