/*
 * <stddef.h> - standard type definitions
 *
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _STDDEF_H
#define _STDDEF_H

#undef NULL
#define NULL (void *) 0

#include <types/ptrdiff_t.h>
#include <types/size_t.h>
#include <types/wchar_t.h>

/*
  I do believe this is straight ripped from musl.
  -Kat
*/
#define offsetof(type, member) (size_t)(&((type *)0->member))

#endif
