/*
 * <assert.h> - make assertions
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */
#include <stdlib.h>
#include <stdio.h>

/* 
  So, this one's a bit weird. Normally, we'd use include guards to make sure
  a header is only included once, but the assert macro is supposed to be
  redefined each time assert.h is included based on the current status of 
  NDEBUG, so we instead undefine-redefine assert and that's it. Technically, 
  it includes stdlib.h and stdio.h every time it's included, but meh. It's fine.
  Those have include guards that mean it doesn't matter.
  -Kat
*/

#ifdef assert
#undef assert
#endif

#ifdef NDEBUG
#define assert(ignore) ((void) 0)
#else
#define assert(expression) (expression || (fprintf(stderr, "Assertion failed: %s: %d: %s", __FILE__, __LINE__, __func__) && abort()))
#endif