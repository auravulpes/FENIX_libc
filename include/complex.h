#ifndef _COMPLEX_H
#define _COMPLEX_H

#define complex _Complex
#define _Complex_I (float _Complex) (1.0fi)
#ifdef __STDC_IEC_559_COMPLEX__
#define imaginary _Imaginary
#define _Imaginary_I (float _Imaginary) (1.0fi)
#endif

#ifdef _Imaginary_I
#define I _Imaginary_I
#else
#define I _Complex_I
#endif

/* Various abs-type stuffs */
/*
  Listen, okay, for some reason, all of these kinda just merge into the
  same category in my head. Do they all do the same thing? No, not really.
  Does that make a difference to me? No. No, it does not.
    -Kat
*/
double cabs(double complex);
float  cabsf(float complex);
long double cabsl(long double complex);

double carg(double complex);
float  cargf(float complex);
long double cargl(long double complex);

double cimag(double complex);
float  cimagf(float complex);
long double cimagl(long double complex);

double complex conj(double complex);
float complex  conjf(float complex);
long double complex conjl(float complex);

double complex cproj(double complex);
float complex  cprojf(float complex);
long double complex cprojl(long double complex);

double creal(double complex);
float  crealf(float complex);
long double creall(long double complex);

/* Trig functions */
double complex ccos(double complex);
float complex  ccosf(float complex);
long double complex ccosl(long double complex);

double complex csin(double complex);
float complex  csinf(float complex);
long double complex csinl(long double complex);

double complex ctan(double complex);
float complex  ctanf(float complex);
long double complex ctanl(long double complex);

/* Inverse trig functions */
double complex cacos(double complex);
float complex  cacosf(float complex);
long double complex cacosl(long double complex);

double complex casin(double complex);
float complex  casinf(float complex);
long double complex casinl(long double complex);

double complex catan(double complex);
float complex  catanf(float complex);
long double complex catanl(long double complex);

/* Hyperbolic trig functions */
double complex ccosh(double complex);
float complex  ccoshf(float complex);
long double complex ccoshl(long double complex);

double complex csinh(double complex);
float complex  csinhf(float complex);
long double complex csinhl(long double complex);

double complex ctanh(double complex);
float complex  ctanhf(float complex);
long double complex ctanhl(long double complex);

/* Hyperbolic inverse trig functions */
double complex cacosh(double complex);
float complex  cacoshf(float complex);
long double complex cacoshl(long double complex);

double complex casinh(double complex);
float complex  casinhf(float complex);
long double complex casinhl(long double complex);

double complex catanh(double complex);
float complex  catanhf(float complex);
long double complex catanhl(long double complex);

/* Various "power triangle" functions */
/*
  Have you ever seen 3blue1brown's video on the power triange? It's
  an interesting way of thinking about how logarithms, exponentials,
  and roots all relate to each other. Anyhoo, these all relate to that.
    -Kat

  (BTW, here's the link: https://www.youtube.com/watch?v=sULa9Lc4pck)
*/
double complex cexp(double complex);
float complex  cexpf(float complex);
long double complex cexpl(long double complex);

double complex clog(double complex); /* Heh. "clog". -Kat */
float complex  clogf(float complex);
long double complex clogl(long double complex);

double complex csqrt(double complex);
float complex  csqrtf(float complex);
long double complex csqrtl(long double complex);

double complex cpow(double complex, double complex);
float complex cpowf(float complex, float complex);
long double complex cpowl(long double complex, long double complex);

#endif