/*
 * <fnmatch.h> - filename matching
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _FNMATCH_H
#define _FNMATCH_H

#define FNM_NOMATCH  1
#define FNM_PATHNAME 2
#define FNM_PERIOD   3
#define FNM_NOESCAPE 4

int fnmatch(const char *, const char *, int);

#endif