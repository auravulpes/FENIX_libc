/*
 * <unistd.h> - standard UNIX stuffs
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _UNISTD_H
#define _UNISTD_H

#ifndef _POSIX_VERSION
#define _POSIX_VERSION 200809L
#endif

#ifndef _POSIX2_VERSION
#define _POSIX2_VERSION 200809L
#endif

#ifndef _XOPEN_VERSION
#define _XOPEN_VERSION 700
#endif

#define _POSIX_ASYNCHRONOUS_IO 200809L
#define _POSIX_BARRIERS 200809L
#define _POSIX_CHOWN_RESTRICTED 0
#define _POSIX_CLOCK_SELECTION 200809L
#define _POSIX_JOB_CONTROL 200809L
#define _POSIX_MAPPED_FILES 200809L
#define _POSIX_MEMORY_PROTECTION 200809L
#define _POSIX_NO_TRUNC 0
#define _POSIX_READER_WRITER_LOCKS 200809L
#define _POSIX_REALTIME_SIGNALS 200809L
#define _POSIX_REGEXP 200809L
#define _POSIX_SAVED_IDS 200809L
#define _POSIX_SEMAPHORES 200809L
#define _POSIX_SHELL 200809L
#define _POSIX_SPIN_LOCKS 200809L
#define _POSIX_THREAD_SAFE_FUNCTIONS 200809L
#define _POSIX_THREADS 200809L
#define _POSIX_TIMEOUTS 200809L
#define _POSIX_TIMERS 200809L

#include <unistd_ilp.h>

#define _POSIX2_C_BIND 200809L

#define _XOPEN_ENH_I18N 700
#define _XOPEN_SHM 700

#include <stddef.h>

#define R_OK 01
#define W_OK 02
#define X_OK 04
#define F_OK 010

#define _CS_PATH "/bin:/sbin:/usr/bin:/usr/sbin"

#define F_LOCK 01
#define F_TEST 02
#define F_TLOCK 03
#define F_ULOCK 04

#include <types/pid_t.h>

pid_t fork(void);
int execv(const char *, char *const []);
int execve(const char *, char *const [], char *const []);
int execvp(const char *, char *const []);

int gethostname(char *, size_t);

#endif
