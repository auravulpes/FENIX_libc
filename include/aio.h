/*
 * <aio.h> - asynchronous IO
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _AIO_H
#define _AIO_H

#include <types/off_t.h>
#include <types/pthread_attr_t.h>
#include <types/size_t.h>
#include <types/ssize_t.h>
#include <time.h>
#include <signal.h>

struct aiocb {
  int aio_fildes;
  off_t aio_offset;
  volatile void * aio_buf;
  size_t aio_nbytes;
  int aio_reqprio;
  struct sigevent aio_sigevent;
  int aio_lio_opcode;
};

#define AIO_ALLDONE 0
#define AIO_CANCELLED 1
#define AIO_NOTCANCELLED 2

#define LIO_NOP 1
#define LIO_READ 2
#define LIO_WRITE 3

#define LIO_NOWAIT 1
#define LIO_WAIT 2

int aio_cancel(int, struct aiocb *);
int aio_error(const struct aiocb *);
int aio_fsync(int, struct aiocb *);
int aio_read(struct aiocb *);
ssize_t aio_return (struct aiocb *);
int aio_suspend(const struct aiocb * const [], int, const struct timespec *);
int aio_write(struct aiocb *);
int lio_listio(int, struct aiocb * restrict const [restrict], int, 
               struct sigevent * restrict);

#endif