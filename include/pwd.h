#ifndef _PWD_H
#define _PWD_H

#include <types/size_t.h>
#include <types/gid_t.h>
#include <types/uid_t.h>

struct passwd {
  char * pw_name;
  uid_t pw_uid;
  gid_t pw_gid;
  char * pw_dir;
  char * pw_shell;
};

#ifdef _XOPEN_SOURCE
struct passwd * getpwent(void);
void setpwent(void);
void endpwent(void);
#endif

struct passwd * getpwnam(const char *);
int getpwnam_r(const char *, struct passwd *, char *, size_t, struct passwd **);
struct passwd * getpwuid(uid_t);
int getpwuid_r(uid_t, struct passwd *, char *, size_t, struct passwd **);

#endif