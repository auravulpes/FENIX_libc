/*
 * <grp.h> - group stuffs
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _GRP_H
#define _GRP_H

#include <types/gid_t.h>
#include <types/size_t.h>

struct group {
  char *  gr_name;
  gid_t   gr_gid;
  char ** gr_mem;
};

void endgrent(void);
struct group * getgrent(void);
struct group * getgrgid(gid_t);
int getgrgid_r(gid_t, struct group *, char *, size_t, struct group **);
struct group * getgrnam(const char *);
int getgrnam_r(const char *, struct group *, char *, size_t, struct group **);
void setgrent(void);

#endif