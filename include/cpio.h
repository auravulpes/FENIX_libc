/*
 * <cpio.h> - values for CPIO archives
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _CPIO_H
#define _CPIO_H

/* Constants needed by the c_mode field of the cpio archive format */
/* User permissions */
#define C_IRUSR 0000400
#define C_IWUSR 0000200
#define C_IXUSR 0000100

/* Group permissions */
#define C_IRGRP 0000040
#define C_IWGRP 0000020
#define C_IXGRP 0000010

/* Other folx permission */
#define C_IROTH 0000004
#define C_IWOTH 0000002
#define C_IXOTH 0000001

#define C_ISUID 0004000 /* Set UID */
#define C_ISGID 0002000 /* Set GID */
#define C_ISVTX 0001000 /* Restricted deletion flag for directories */

#define C_ISFIFO 0010000 /* FIFO/Pipe */
#define C_ISREG 0100000 /* Regular file */
#define C_ISBLK 0060000 /* Block device */
#define C_ISCHR 0020000 /* Character special */
#define C_ISCTG 0110000 /* Reserved */
#define C_ISLNK 0120000 /* Symbolic link */
#define C_ISSOCK 0140000 /* Socket */

#define MAGIC "070707"

#endif
