/*
 * <time.h> - time stuffs
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _TIME_H
#define _TIME_H

#ifdef _XOPEN_SOURCE
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200908L
#endif
#endif

#ifndef NULL
#define NULL (void *) 0
#endif

#include <types/clock_t.h>
#include <types/size_t.h>
#include <types/time_t.h>
#ifdef _POSIX_C_SOURCE
#include <types/clockid_t.h>
#include <types/timer_t.h>
#include <types/pid_t.h>
struct sigevent;
#include <types/locale_t.h>
#endif

struct tm {
  unsigned int tm_sec : 6;
  unsigned int tm_min : 6;
  unsigned int tm_hour : 5;
  unsigned int tm_mday : 5;
  unsigned int tm_mon : 4;
  unsigned int tm_year;
  unsigned int tm_wday : 3;
  unsigned int tm_yday : 9;
  int tm_isdst : 2;
};

#ifdef _POSIX_C_SOURCE
struct timespec {
  time_t tv_sec;
  long int tv_nsec;
};

struct itimerspec {
  struct timespec it_interval;
  struct timespec it_value;
};
#endif

#define CLOCKS_PER_SEC ((clock_t) 1000000)

#ifdef _POSIX_C_SOURCE
#define CLOCK_MONOTONIC 01
#define CLOCK_PROCESS_CPUTIME_ID 02
#define CLOCK_REALTIME 04
#define CLOCK_THREAD_CPUTIME_ID 010

#define TIMER_ABS_TIME 1
#endif

#ifdef _XOPEN_SOURCE
extern int getdate_err;
extern int daylight;
extern long timezone;
#endif

clock_t clock(void);
time_t time(time_t *);
double difftime(time_t, time_t);
time_t mktime(struct tm *);
int timespec_get(struct timespec *, int);
char * asctime(const struct tm *);
char * ctime(const time_t *);
struct tm * gmtime(const time_t *);
struct tm * localtime(const time_t *);
size_t strftime(char * restrict, size_t, const char * restrict, 
                const struct tm * restrict);


#ifdef _POSIX_C_SOURCE
char * asctime_r(const struct tm * restrict, char * restrict);
int clock_getcpuclockid(pid_t, clockid_t *);
int clock_getres(clockid_t, struct timespec *);
int clock_gettime(clockid_t, struct timespec *);
int clock_nanosleep(clockid_t, int, const struct timespec *, struct timespec *);
int clock_settime(clockid_t, const struct timespec *);
char * ctime_r(const time_t *, char *);
struct tm * gmtime_r(const time_t * restrict, struct tm * restrict);
struct tm * localtime_r(const time_t * restrict, struct tm * restrict);
int nanosleep(const struct timespec *, struct timespec *);
size_t strftime_l(char * restrict, size_t, const char * restrict,
                  const struct tm * restrict, locale_t);
int timer_create(clockid_t, struct sigevent * restrict, timer_t * restrict);
int timer_delete(timer_t);
int timer_getoverrun(timer_t);
int timer_gettime(timer_t, struct itimerspec *);
int timer_settime(timer_t, int, const struct itimerspec * restrict, 
                  struct itimerspec * restrict);
void tzset(void);
#endif

#ifdef _XOPEN_SOURCE
struct time * getdate(const char *);
char * strptime(const char * restrict, const char * restrict, 
                struct tm * restrict);
#endif

#endif /* not _HEADER */
