/*
 * <stdlib.h> - standard library definitions
 * 
 * This header is a part of the FENIX C Library and is free software.
 * You can redistribute and/or modify it subject to the terms of the
 * Clumsy Wolf Public License v4. For more details, see the file COPYING.
 *
 * The FENIX C Library is distributed WITH NO WARRANTY WHATSOEVER. See
 * The CWPL for more details.
 */

#ifndef _STDLIB_H
#define _STDLIB_H

#ifdef _XOPEN_SOURCE
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif
#endif

#include <sys/cdefs.h>

#ifndef NULL
#define NULL (void *) 0
#endif

#include <types/size_t.h>
#include <types/wchar_t.h>

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

#define RAND_MAX 2147483647

#define MB_CUR_MAX (size_t) 1

struct div_t {
  int quot;
  int rem;
};

struct ldiv_t {
  long quot;
  long rem;
};

struct lldiv_t {
  long long quot;
  long long rem;
};

#ifdef _POSIX_C_SOURCE
#include <sys/wait.h>
#endif

void abort(void);
int atexit(void (*)(void));
void exit(int);
void _Exit(int);

char * getenv(const char *);
int system(const char *);

void * malloc(size_t);
void * calloc(size_t, size_t);
void * realloc(void *, size_t);
void free(void *);

void srand(unsigned int);
int rand(void);

double atof(const char *);
int atoi(const char *);
long int atol(const char *);
long long int atoll(const char *);

double strtod(const char * restrict, char ** restrict);
float strtof(const char * restrict, char ** restrict);
long double strtold(const char * restrict, char ** restrict);
long int strtol(const char * restrict, char ** restrict, int);
long long int strtoll(const char * restrict, char ** restrict, int);
unsigned long int strtoul(const char * restrict, char ** restrict, int);
unsigned long long int strtoull(const char * restrict, char ** restrict, int);

struct div_t div(int, int);
struct ldiv_t ldiv(long, long);
struct lldiv_t lldiv(long long, long long);

int abs(int);
long labs(long);
long long llabs(long long);

void * bsearch(const void *, const void *, size_t, size_t,
	             int (*)(const void *, const void *));
void qsort(void *, size_t, size_t, int(*)(const void *, const void *));

int mblen(const char *, size_t);
int mbtowc(wchar_t * restrict, const char * restrict, size_t);
int wctomb(char *, wchar_t);

size_t mbstowcs(wchar_t * restrict, const char * restrict, size_t);
size_t wcstombs(char * restrict, const wchar_t * restrict, size_t);

#endif
