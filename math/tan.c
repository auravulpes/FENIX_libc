#include <math.h>

double tan(double x) {
  return (cos(x - M_PI_2) / cos(x));
}