#include <math.h>

long double fabsl(long double x) {
  if(x < 0) {
    return -x;
  }
  else {
    return x;
  }
}
