#include <math.h>

float fabsf(float x) {
  if(x < 0) {
    return -x;
  }
  else {
    return x;
  }
}
