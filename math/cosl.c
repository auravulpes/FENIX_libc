#include <math.h>

long double cosl(long double x) {
  long double pi = M_PI;
  int temp;
  long double deg_2, deg_4, deg_6, deg_8, deg_10;
  long double deg_12, deg_14, deg_16, deg_18, deg_20;
  long double cosine;
  
  if(x < 0) x = -x;

  if(x >= 2 * pi || x <= -2 * pi) {
    temp = x / (2 * pi);
    x -= temp * (2 * pi);
  }

  deg_2 = (x - pi) * (x - pi) / 2;
  deg_4 = deg_2 * deg_2 * 2 / (4 * 3);
  deg_6 = deg_4 * deg_2 * 2 / (6 * 5);
  deg_8 = deg_6 * deg_2 * 2 / (8 * 7);
  deg_10 = deg_8 * deg_2 * 2 / (10 * 9);
  deg_12 = deg_10 * deg_2 * 2 / (12 * 11);
  deg_14 = deg_12 * deg_2 * 2 / (14 * 13);
  deg_16 = deg_14 * deg_2 * 2 / (16 * 15);
  deg_18 = deg_16 * deg_2 * 2 / (18 * 17);
  deg_20 = deg_18 * deg_2 * 2 / (20 * 19);

  cosine = -1 + deg_2 - deg_4 + deg_6 - deg_8 + deg_10;
  cosine = cosine - deg_12 + deg_14 - deg_16 + deg_18 + deg_20;

  return cosine;
}
