#include <math.h>

double sin(double x) {
  return cos(x - M_PI_2);
}