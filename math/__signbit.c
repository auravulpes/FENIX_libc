#include <math.h>

int __signbitd(double x) {
  union fp {
    double d;
    long i;
  } f;

  f.d = x;

  return f.i & 0x8000000000000000;
}

int __signbitf(float x) {
  union fp {
    float f;
    int i;
  } f;

  f.f = x;

  return f.i & 0x80000000;
}

int __signbitl(long double x) {
  union fp {
    long double d;
    long long i;
  } f;

  f.d = x;

  return f.i & 0x80000000000000000000000000000000;
}