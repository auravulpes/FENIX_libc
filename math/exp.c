#include <math.h>

#define TOLERANCE 0.0000001

/*
  A C adaptation of Dr. Ching-Kuang Shene's FORTRAN version
  from https://pages.mtu.edu/~shene/COURSES/cs201/NOTES/chap04/exp.html

  I was stuck on how to actually implement the Taylor series without it
  getting far too big for even a long double, and I'm not sure I ever would
  have thought to just break it out recursively like this. Instead of each
  term being x^n/n!, where we have to figure out both x^n and n!, both of which
  could be massive, we instead say that it's (x/n+1)*(nth term), with a base 
  case (1st term) of x/1! = x. Pretty clever, really.

  Also, really glad I know how to at least read a basic FORTRAN program. This
  probably would have been a little more annoying if I didn't.

  -Kat
*/
double exp(double x) {
  double term = x;
  double sum = 1.0;

  for(int i = 1; fabs(term) > TOLERANCE; i++) {
    sum += term;
    term *= (x / i);
  }

  return sum;
}