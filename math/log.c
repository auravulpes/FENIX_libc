#include <math.h>

double _agm(double x, double y) {
  double a = x;
  double g = y;
  double temp;

  for(int i = 0; i < 15; i++) {
    temp = 0.5 * (a + g);
    g = sqrt(a * g);
    a = temp;
  }

  return a;
}

double log(double x) {
  double s = x * 16777216;
  double nat_log = (M_PI / (2 * _agm(1, 4/s))) - (24 * M_LN2);
  return nat_log;
}