#include <sys/utsname.h>
#include <kernel/syscall.h>
#include <unistd.h>

int uname(struct utsname * u) {
  __syscall_uname(u);
  gethostname(u->nodename, 70);
  return 1;
}