#include <stdlib.h>

struct ldiv_t ldiv(long int x, long int y) {
  struct ldiv_t ret_val;
  ret_val.quot = x / y;
  ret_val.rem  = x % y;
  return ret_val;
}
