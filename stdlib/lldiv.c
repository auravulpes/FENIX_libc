#include <stdlib.h>

struct lldiv_t lldiv(long long int x, long long int y) {
  struct lldiv_t ret_val;
  ret_val.quot = x / y;
  ret_val.rem  = x % y;
  return ret_val;
}
