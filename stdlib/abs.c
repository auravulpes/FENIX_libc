#include <stdlib.h>

int abs(int x) {
  int y = x < 0 ? -x : x;
  return y;
}
