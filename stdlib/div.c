#include <stdlib.h>

struct div_t div(int x, int y) {
  struct div_t ret_val;
  ret_val.quot = x / y;
  ret_val.rem  = x % y;
  return ret_val;
}
