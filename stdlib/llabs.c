#include <stdlib.h>

long long int llabs(long long int x) {
  long long int y = x < 0 ? -x : x;
  return y;
}
