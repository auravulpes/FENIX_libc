#include <stdlib.h>

long int labs(long int x) {
  long int y = x < 0 ? -x : x;
  return y;
}
