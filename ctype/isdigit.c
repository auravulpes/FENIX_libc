#include <ctype.h>

int isdigit(int c) {
  return (c <= '9' && c >= '0');
}