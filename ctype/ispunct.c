#include <ctype.h>

int ispunct(int c) {
  return (c < 128 && !(isalnum(c) || iscntrl(c)));
}