#include <string.h>

/*
  This might need to be rewritten. It does grow as
  O(m*n), where m and n are the number of characters
  in s2 and s1. If I do, musl's implementation is
  kinda cool. I could do something like that. But,
  I think it'll do for now.
  -Kat
*/

size_t strcspn(const char * s1, const char * s2) {
  int i = 0;
  for(i = 0; s1[i] != '\0'; i++) {
    for(int j = 0; s2[j] != '\0'; j++) {
      if(s1[i] == s2[j]) {
	return i;
      }
    }
  }
  return i;
}
