#include <string.h>

void * memmove(void * dest, const void * src, size_t size) {
  unsigned char * to = (unsigned char *) dest;
  const unsigned char * from = (const unsigned char *) src;

  /* We check this to make sure we don't overwrite memory */
  if(to < from) {
    for(size_t i = 0; i < size; i++) {
      to[i] = from[i];
    }
  }
  else {
    for(size_t i = size; i != 0; i--) {
      to[i - 1] = from[i - 1];
    }
  }
  
  return dest;
}
