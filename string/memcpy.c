#include <string.h>

void * memcpy(void * restrict dest, const void * restrict src, size_t size) {
  unsigned char * to = (unsigned char *) dest;
  const unsigned char * from = (const unsigned char *) src;

  for(size_t i = 0; i < size; i++) {
    to[i] = from[i];
  }

  return dest;
}
