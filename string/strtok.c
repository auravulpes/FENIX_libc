#include <string.h>

char * strtok(char * restrict str, char * restrict sep) {
  static char * state;
  char * search_str;
  int i = 0, sep_size = strlen(sep);

  if(str != NULL) {
    search_str = str;
  }
  else {
    search_str = state;
  }

  while(*search_str != '\0' && strcmp(search_str, sep) == 0) {
    search_str += sep_size;
  }

  if(*search_str == '\0') {
    return NULL;
  }

  while(search_str[i] != '\0' && strcmp(&(search_str[i]), sep) != 0) {
    i++;
  }

  search_str[i] = '\0';
  state = &(search_str[i + 1]);
  return search_str;
}
