#include <string.h>

char * strcpy(char * restrict dest, const char * restrict src) {
  char * to = dest;
  const char * from = src;
  size_t i;

  for(i = 0; from[i] != '\0'; i++) {
    to[i] = from[i];
  }

  to[i] = from[i];

  return dest;
}
