#include <string.h>

/*
  Is this...okay? Like, from what I can tell, it should be,
  since s1 and s2 are restricted. s1 and s2 shouldn't be the
  same object. But, like, I'm worried about what happens if
  someone passes in the same pointer for s1 and s2. You'd get
  an infinite loop of adding stuff that just eats up all
  memory from there! But, like, I think musl does the same
  thing? I really can't tell. For now, I'm gonna assume I've
  done this acceptably.
  For now.
  -Kat
*/

/*
  I'm pretty sure musl does the same thing, for the most part.
  -Kat
*/

/*
  Does weird stuff. Maybe don't use until we figure out what
  the fuck is wrong with it.
  -Kat
 */

char * strcat(char * restrict s1, const char * restrict s2) {
  int i = 0, j;
  for(i = 0; s1[i] != '\0'; i++){}
  for(j = 0; s2[j] != '\0'; i++, j++) {
    s1[i] = s2[j];
  }
  s1[i] = '\0';

  return s1;
}
