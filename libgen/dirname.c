#include <libgen.h>

char * dirname(char * path) {
  int i, last_slash = -1, length;
  char * dirname; char c;

  if(path == NULL || strcmp(path, "") == 0) {
    dirname = calloc(2, sizeof(*dirname));
    if(dirname == NULL) {
      errno = ENOMEM;
      return NULL;
    }
    strcpy(dirname, ".");
    return dirname;
  }

  length = strlen(path);

  if(path[length] == '/') {
    path[length] = '\0';
  }

  i = 0;
  for(c = path[i]; c != '\0'; c = path[++i]) {
    if(c == '/') {
      last_slash = i;
    }
  }

  dirname = calloc(last_slash + 2, sizeof(*dirname));
  if(dirname == NULL) {
    errno = ENOMEM;
    return NULL;
  }

  for(i = 0; i <= last_slash; i++) {
    dirname[i] = path[i];
  }
  dirname[i] = '\0';

  return dirname;
}
