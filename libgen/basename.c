#include <libgen.h>

char * basename(char * path) {
  char * basename; char c;
  int basename_length = 0, last_slash = 0;
  int i = 0, j = 0;

  if(path == NULL || strcmp(path, "") == 0) {
    basename = calloc(2, sizeof(*basename));
    if(basename == NULL) {
      errno = ENOMEM;
      return NULL;
    }
    strcpy(basename, ".");
    return basename;
  }
  
  if(strcmp(path, "/") == 0) {
    basename = calloc(2, sizeof(*basename));
    if(basename == NULL) {
      errno = ENOMEM;
      return NULL;
    }
    strcpy(basename, "/");
    return basename;
  }

  if(strcmp(path, "//") == 0) {
    basename = calloc(2, sizeof(*basename));
    if(basename == NULL) {
      errno = ENOMEM;
      return NULL;
    }
    strcpy(basename, "/");
    return basename;
  }

  i = strlen(path);
  if(path[i] == '/') {
    path[i] = '\0';
  }

  i = 0;
  for(c = path[i]; c != '\0'; c = path[i]) {
    if(c == '/') {
      last_slash = i;
    }
    i++;
  }

  i = last_slash + 1;
  for(c = path[i]; c != '\0'; c = path[i]) {
    basename_length++; i++;
  }

  basename = calloc(basename_length + 1, sizeof(*basename));
  if(basename == NULL) {
    errno = ENOMEM;
    return NULL;
  }

  i = last_slash + 1;
  j = 0;
  for(c = path[i]; c != '\0'; c = path[i]) {
    basename[j] = c;
    i++; j++;
  }
  basename[j] = '\0';

  return basename;
}
